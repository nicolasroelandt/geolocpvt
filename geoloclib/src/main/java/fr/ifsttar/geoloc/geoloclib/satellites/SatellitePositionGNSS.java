///=================================================================================================
// Class SatellitePositionGNSS
//      Author :  Antoine GRENIER - 2019/09/06
//        Date :  2019/09/06
///=================================================================================================
/*
 * Copyright 2018(c) IFSTTAR - TeamGEOLOC
 *
 * This file is part of the GeolocPVT application.
 *
 * GeolocPVT is distributed as a free software in order to build a community of users, contributors,
 * developers who will contribute to the project and ensure the necessary means for its evolution.
 *
 * GeolocPVT is a free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version. Any modification of source code in this
 * LGPL software must also be published under the LGPL license.
 *
 * GeolocPVT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * You should have received a copy of the GNU Lesser General Public License along with GeolocPVT.
 * If not, see <https://www.gnu.org/licenses/lgpl.txt/>.
 */
///=================================================================================================
package fr.ifsttar.geoloc.geoloclib.satellites;

import android.location.GnssStatus;

import org.ejml.simple.SimpleMatrix;
import org.gogpsproject.Constants;
import org.gogpsproject.ephemeris.PreciseCorrection;
import org.gogpsproject.ephemeris.GNSSEphemeris;

import java.io.IOException;

import fr.ifsttar.geoloc.geoloclib.Coordinates;
import fr.ifsttar.geoloc.geoloclib.LatLngAlt;
import fr.ifsttar.geoloc.geoloclib.Options;
import fr.ifsttar.geoloc.geoloclib.Utils;

/**
 * A class to compute the coordinates of the satellites
 * Based on SatellitePositionsGPS class.
 *
 * References: [1] RTCM State Space Representation - Messages, Status and Plans, Martin Schmitz, Geo++.
 */
public class SatellitePositionGNSS
{
    private GNSSEphemeris eph;
    private PreciseCorrection ephCorrections;

    private Coordinates satCoordinates;
    private Coordinates prevSatCoordinates;
    private Coordinates userCoord;
    private double satElevation;

    private int wn;
    private double tRecep;
    private double tEmis;
    private double dtSat;
    private double geomTransmissionTime;

    // Precise corrections
    private SimpleMatrix dX_precise;
    private double dC_precise;

    // Orbital parameters
    private double uk;
    private double ik;
    private double OMEGAk;

    private final double DELTA_T = 1.0;     // [s], time between two measurements

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor.
     * @param coordinates Satellites coordinates
     * @param deltaTsv Satellite clock bias
     * @param userCoord User coordinates
     */
    public SatellitePositionGNSS(Coordinates coordinates, Double deltaTsv, Coordinates userCoord)
    {
        this.satCoordinates = coordinates;
        this.userCoord = userCoord;
        this.dtSat = deltaTsv;

        computeSatelliteElevation();
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor (For EphemerisGPS object).
     * @param ephGps GPS satellite ephemeris
     * @param time Reception time
     * @param ecefUserCoord User coordinates
     */
    public SatellitePositionGNSS(EphemerisGPS ephGps, double time, Coordinates ecefUserCoord)
    {
        wn = ephGps.getWn();
        tRecep = time;
        this.userCoord = ecefUserCoord;

        this.eph = eph;

        computeSatellitePosition(tRecep);
        computeSatelliteElevation();

        this.geomTransmissionTime = satCoordinates.getSimpleMatrix().minus(userCoord.getSimpleMatrix()).normF()
                / Constants.SPEED_OF_LIGHT;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Constructor.
     * @param eph Satellite ephemeris
     * @param time Reception time
     * @param ecefUserCoord User coordinates
     * @param processingOptions Computation options
     */
    public SatellitePositionGNSS(GNSSEphemeris eph, double time, Coordinates ecefUserCoord, Options processingOptions)
    {
        wn = eph.getWn();
        tRecep = time;
        this.userCoord = ecefUserCoord;

        this.eph = eph;

        satCoordinates = computeSatellitePosition(tRecep);
        computeSatelliteElevation();

        // Computing satellites coordinates at the previous epoch
        prevSatCoordinates = computeSatellitePosition(tRecep - DELTA_T);

        this.geomTransmissionTime = satCoordinates.getSimpleMatrix().minus(userCoord.getSimpleMatrix()).normF()
                / Constants.SPEED_OF_LIGHT;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Compute the satellite position using the broadcast ephemeris parameters.
     * @param tRecep Reception time already corrected of the earth rotation effect.
     */
    public Coordinates computeSatellitePosition(double tRecep)
    {
        if(eph.getGnssSystem() != GnssStatus.CONSTELLATION_GPS
                && eph.getGnssSystem() != GnssStatus.CONSTELLATION_GALILEO
                && eph.getGnssSystem() != GnssStatus.CONSTELLATION_BEIDOU)
        {
            return null;
        }

        /// Difference to ephemeris time
        double dt = tRecep - eph.getToe();

        //beginning and end of week crpréciseossovers correction
        if(dt > 302400)
        {
            dt -= 604800;
        }
        else if(dt < -302400)
        {
            dt += 604800;
        }

        double a = Math.pow(eph.getSquareA(),2);
        double e = eph.getEc();

        // Mean motion
        double n0 = Math.sqrt(Constants.EARTH_GRAVITATIONAL_CONSTANT/Math.pow(a,3));
        double n = n0 + eph.getDeltaN();

        // Mean anomaly
        double M = eph.getM0() + n * dt;
        double MDot = n;

        // Eccentric anomaly
        double E = M;
        double EDot = MDot / (1 - e * Math.cos(E));
        double diff = E;
        while(Math.abs(diff)>1.0e-13)
        {
            diff = E;
            E = M + e * Math.sin(E);
            MDot = (1 - e * Math.cos(E)) * EDot;
            EDot = MDot / (1 - e * Math.cos(E));
            diff -= E;
        }

        // Compute true anomaly
        double v;
        double vDot;
        v = Math.atan2(Math.sqrt(1 - Math.pow(e,2)) * Math.sin(E), (Math.cos(E) - e));
        vDot = Math.sin(E) * EDot * (1 + e * Math.cos(v)) / ((1 - Math.cos(E) * e) * Math.sin(v));

        // Argument of latitude
        double phi = v + eph.getOmega();
        double phiDot = vDot;
        double u = eph.getOmega() + v + eph.getCus() * Math.sin(2*phi) + eph.getCuc() * Math.cos(2*phi);
        double duDot = 2 * (eph.getCus() * Math.cos(2*phi) - eph.getCuc() * Math.sin(2*phi)) * phiDot;
        double uDot = phiDot + duDot;

        // Radius
        double r = a * (1 - e * Math.cos(E));
        double rDot = a * e * Math.sin(E) * EDot;
        double dr = eph.getCrs() * Math.sin(2*phi) + eph.getCrc() * Math.cos(2*phi);
        double drDot = 2 * (eph.getCrs() * Math.cos(2*phi) - eph.getCrc() * Math.sin(2*phi)) * phiDot;
        r += dr;
        rDot += drDot;

        // Inclination
        double i;
        double diDot = 2 * (eph.getCis() * Math.cos(2*phi) - eph.getCic() * Math.sin(2*phi)) * phiDot;
        double iDot = eph.getIdot() + diDot;
        i = eph.getI0() + eph.getIdot() * dt;
        i += eph.getCis() * Math.sin(2*phi) + eph.getCic() * Math.cos(2*phi);

        // Longitude of ascending node
        double OMEGA;
        double OMEDADot = (eph.getOmegaDot() - Constants.OMEGAE_DOT_GPS);
        OMEGA = eph.getOmega0() + (eph.getOmegaDot() - Constants.OMEGAE_DOT_GPS) * dt;
        OMEGA -= Constants.OMEGAE_DOT_GPS * eph.getToe();

        // Saving orbital parameters for future computations
        uk = u;
        ik = i;
        OMEGAk = OMEGA;

        // ECEF coordinates
        double xp = r * Math.cos(u);
        double yp = r * Math.sin(u);

        double x = xp * Math.cos(OMEGA) - yp * Math.cos(i) * Math.sin(OMEGA);
        double y = xp * Math.sin(OMEGA) + yp * Math.cos(i) * Math.cos(OMEGA);
        double z = yp * Math.sin(i);

        double xDotp = rDot * Math.cos(u) - yp * uDot;
        double yDotp = rDot * Math.sin(u) + xp * uDot;

        // ECEF velocities;
        // TODO Verify the velocities
        double xDot = 0.0;
        xDot += xDotp * Math.cos(OMEGA) - yDotp * Math.cos(i) * Math.sin(OMEGA);
        xDot += yp * Math.sin(i) * Math.sin(OMEGA) * iDot - y * OMEDADot;

        double yDot = 0.0;
        yDot += xDotp * Math.sin(OMEGA) + yDotp * Math.cos(i) * Math.cos(OMEGA);
        yDot += -yp * Math.sin(i) * iDot * Math.cos(OMEGA) + x * OMEDADot;

        double zDot = yDotp * Math.sin(i) + yp * Math.cos(i) * iDot;

        /// Satellites clock error
        double _dtSat = eph.getAf0() + eph.getAf1() * dt + eph.getAf2() * Math.pow(dt, 2);
        // Relativist clock correction
        _dtSat -= Constants.RELATIVISTIC_ERROR_CONSTANT * eph.getSquareA() * e * Math.sin(E);

        this.tEmis = tRecep - _dtSat;
        this.dtSat = _dtSat;
        return new Coordinates(x, y, z, xDot, yDot, zDot, tEmis);
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Compute the satellite elevation, based on GNSS Compare algorithm.
     */
    public void computeSatelliteElevation()
    {
        SimpleMatrix enu = getENU();

        double elevation = 0.0;

        double E = enu.get(0,0);//enu.get(0);
        double N = enu.get(1,0);//enu.get(1);
        double U = enu.get(2,0);//enu.get(2);

        // Compute horizontal distance from origin to this object
        double hDist = Math.sqrt(Math.pow(E, 2) + Math.pow(N, 2));

        // If this object is at zenith ...
        if (hDist < 1e-20) {
            // ... set azimuth = 0 and elevation = 90, ...
            elevation = 90;

        } else {
            // ... and elevation
            elevation = Math.toDegrees(Math.atan2(U, hDist));
        }

        //Log.i("ELEV", "" + id + ", " + elevation);

        this.satElevation = elevation;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Get ENU coordinates of the satellite relative to the given position.
     * Taken from the GNSS Compare library.
     */
    private SimpleMatrix getENU()
    {
        Coordinates ecefUserCoord = this.userCoord;
        Coordinates coordinates = this.satCoordinates;

        SimpleMatrix rSat = coordinates.getSimpleMatrix();
        SimpleMatrix rUser = ecefUserCoord.getSimpleMatrix();
        SimpleMatrix diffVec;
        SimpleMatrix enu;

        diffVec = rSat.minus(rUser);

        LatLngAlt geoUserCoord = new LatLngAlt(ecefUserCoord);

        //Compute rotation matrix from ECEF to ENU

        double lam = Math.toRadians(geoUserCoord.getLongitude());
        double phi = Math.toRadians(geoUserCoord.getLatitude());

        double cosLam = Math.cos(lam);
        double cosPhi = Math.cos(phi);
        double sinLam = Math.sin(lam);
        double sinPhi = Math.sin(phi);

        double[][] data = new double[3][3];
        data[0][0] = -sinLam;
        data[0][1] = cosLam;
        data[0][2] = 0;
        data[1][0] = -sinPhi * cosLam;
        data[1][1] = -sinPhi * sinLam;
        data[1][2] = cosPhi;
        data[2][0] = cosPhi * cosLam;
        data[2][1] = cosPhi * sinLam;
        data[2][2] = sinPhi;

        SimpleMatrix R = new SimpleMatrix(data);

        //Compute ENU coordinates of satellite
        enu = R.mult(diffVec);

        return enu;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Function applying corrections to orbit. Reference [1]
     */
    public void applyPreciseCorrections()
    {
        ephCorrections = eph.getEphCorrections();

        if(ephCorrections == null)
        {
            return;
        }

        double delta_tk = tEmis - (ephCorrections.getTow() + ephCorrections.getUpdateInterval() / 2);

        // Ephemeris corrections
        SimpleMatrix X    = new SimpleMatrix(3, 1);
        SimpleMatrix xDot = new SimpleMatrix(3, 1);
        SimpleMatrix dO   = new SimpleMatrix(3, 1);

        SimpleMatrix eRadial;
        SimpleMatrix eAlong;
        SimpleMatrix eCross;

        double oRadial = ephCorrections.geteRadial();
        double oAlong = ephCorrections.geteAlong();
        double oCross = ephCorrections.geteCross();

        double oDotRadial = ephCorrections.geteDotRadial();
        double oDotAlong = ephCorrections.geteDotAlong();
        double oDotCross = ephCorrections.geteDotCross();

        dO.set(0,0,oRadial + oDotRadial * delta_tk);
        dO.set(1,0, oAlong + oDotAlong * delta_tk);
        dO.set(2,0, oCross + oDotCross * delta_tk);

        X.set(0, 0, satCoordinates.getX());
        X.set(1, 0, satCoordinates.getY());
        X.set(2, 0, satCoordinates.getZ());

        // Setting satellites velocity
        xDot = new SimpleMatrix(3,1);
        xDot.set(0,0, satCoordinates.getxDot());
        xDot.set(1,0, satCoordinates.getyDot());
        xDot.set(2,0, satCoordinates.getzDot());

        // Compute dX
        eAlong = xDot.divide(xDot.normF());
        eCross = Utils.crossMult(X, xDot).divide(Utils.crossMult(X, xDot).normF());
        eRadial = Utils.crossMult(eAlong, eCross);

        dX_precise = concat3Vec(eRadial, eAlong, eCross).mult(dO);

        X.set(0,0,X.get(0,0) - dX_precise.get(0,0));
        X.set(1,0,X.get(1,0) - dX_precise.get(1,0));
        X.set(2,0,X.get(2,0) - dX_precise.get(2,0));

        // Clocks corrections
        dC_precise = ephCorrections.getC0()
                + ephCorrections.getC1() * delta_tk
                + ephCorrections.getC2() * Math.pow(delta_tk, 2);

        dC_precise /= Constants.SPEED_OF_LIGHT;

        tEmis -= dC_precise;
        dtSat -= dC_precise;

        satCoordinates = new Coordinates(X.get(0,0), X.get(1,0), X.get(2,0),
                satCoordinates.getxDot(), satCoordinates.getyDot(), satCoordinates.getzDot(), tEmis);
    }

    /**
     * Function to concatenate 3 column vector into a 3x3 matrix
     * @param A 1st vector
     * @param B 2nd vector
     * @param C 3rd vector
     * @return Concatenated matrix
     */
    private SimpleMatrix concat3Vec(SimpleMatrix A, SimpleMatrix B, SimpleMatrix C)
    {
        SimpleMatrix D = new SimpleMatrix(3,3);

        D.set(0,0, A.get(0,0));
        D.set(1,0, A.get(1,0));
        D.set(2,0, A.get(2,0));

        D.set(0,1, B.get(0,0));
        D.set(1,1, B.get(1,0));
        D.set(2,1, B.get(2,0));

        D.set(0,2, C.get(0,0));
        D.set(1,2, C.get(1,0));
        D.set(2,2, C.get(2,0));

        return D;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Compute the rotation matrix to go from satellite body-frame to ECEF-frame
     * @return Rotation matrix
     */
    private SimpleMatrix getRotationMatrix()
    {
        SimpleMatrix Ri = new SimpleMatrix(3,3);
        SimpleMatrix ROMEGA = new SimpleMatrix(3,3);
        SimpleMatrix Romega = new SimpleMatrix(3,3);

        SimpleMatrix R;

        ROMEGA.set(0,0,  Math.cos(OMEGAk));
        ROMEGA.set(0,1, -Math.sin(OMEGAk));
        ROMEGA.set(1,0,  Math.sin(OMEGAk));
        ROMEGA.set(1,1,  Math.cos(OMEGAk));
        ROMEGA.set(2,2, 1);

        Ri.set(0,0, 1);
        Ri.set(1,1,  Math.cos(ik));
        Ri.set(1,2, -Math.sin(ik));
        Ri.set(2,1,  Math.sin(ik));
        Ri.set(2,2,  Math.cos(ik));

        Romega.set(0,0,  Math.cos(uk));
        Romega.set(0,1, -Math.sin(uk));
        Romega.set(1,0,  Math.sin(uk));
        Romega.set(1,1,  Math.cos(uk));
        Romega.set(2,2,  1);

        R = ROMEGA.mult(Ri).mult(Romega);

        return R;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Apply the correction for Earth rotation during transmission time.
     * @param dt Transmission time
     */
    public void applyEarthRotation(double dt)
    {
        // Current satellite coordinates
        SimpleMatrix X = new SimpleMatrix(3,1);
        SimpleMatrix XCorr;
        SimpleMatrix R3;

        R3 = getRotationR3(-Constants.EARTH_ANGULAR_VELOCITY * dt);

        X.set(0, 0, satCoordinates.getX());
        X.set(1, 0, satCoordinates.getY());
        X.set(2, 0, satCoordinates.getZ());

        XCorr = R3.mult(X);

        satCoordinates = new Coordinates(XCorr.get(0,0), XCorr.get(1,0), XCorr.get(2,0),
                satCoordinates.getxDot(), satCoordinates.getyDot(), satCoordinates.getzDot(), tEmis);

        // Previous epoch satellite coordinates
        X.set(0, 0, prevSatCoordinates.getX());
        X.set(1, 0, prevSatCoordinates.getY());
        X.set(2, 0, prevSatCoordinates.getZ());

        XCorr = R3.mult(X);

        prevSatCoordinates = new Coordinates(XCorr.get(0,0), XCorr.get(1,0), XCorr.get(2,0),
                prevSatCoordinates.getxDot(), prevSatCoordinates.getyDot(), prevSatCoordinates.getzDot(), tEmis-1);
    }

    //----------------------------------------------------------------------------------------------

    /**
     * Compute the rotation matrix R3 (around the z-axis).
     * @param angle Rotation angle
     */
    private SimpleMatrix getRotationR3(double angle)
    {
        SimpleMatrix R3 = new SimpleMatrix(3,3);

        R3.set(0,0,  Math.cos(angle));
        R3.set(0,1, -Math.sin(angle));
        R3.set(1,0,  Math.sin(angle));
        R3.set(1,1,  Math.cos(angle));
        R3.set(2,2, 1);

        return R3;
    }

    //----------------------------------------------------------------------------------------------

    public void setEphCorrections(PreciseCorrection ephCorrections) {
        this.ephCorrections = ephCorrections;
    }

    //----------------------------------------------------------------------------------------------

    public GNSSEphemeris getEph() {
        return eph;
    }

    //----------------------------------------------------------------------------------------------

    public Coordinates getSatCoordinates() {
        return satCoordinates;
    }

    //----------------------------------------------------------------------------------------------

    public double getGeomTransmissionTime() {
        return geomTransmissionTime;
    }

    //----------------------------------------------------------------------------------------------

    public double getSatElevation() {
        return satElevation;
    }

    //----------------------------------------------------------------------------------------------

    public double getDtSat() {
        return dtSat;
    }

    //----------------------------------------------------------------------------------------------

    public Coordinates getPrevSatCoordinates() {
        return prevSatCoordinates;
    }

    //----------------------------------------------------------------------------------------------

    public SimpleMatrix getdX_precise() {
        return dX_precise;
    }

    //----------------------------------------------------------------------------------------------

    public double getdC_precise() {
        return dC_precise;
    }

    //----------------------------------------------------------------------------------------------

    public static void main(String[] args) throws IOException
    {
        double tow = 383400;

        //Coordinates trueSatCoordinate = new Coordinates(-18416679.678594064, -13791301.967686517, -13737154.229846274);
        Coordinates trueSatCoordinate = new Coordinates(-18416750.07578751, -13791207.95992266, -13737154.229846274);
        Coordinates approxUserCoord = new Coordinates(4343426.0, -124910.0, 4653463.0);
        Options options = new Options();

        GNSSEphemeris eph = new GNSSEphemeris();

        eph.setGnssSystem(1);
        eph.setPrn(1);
        eph.setToe(381584.0);
        eph.setAf0(-3.99947166443e-05);
        eph.setAf1(-9.66338120634e-12);
        eph.setAf2(0.0);
        eph.setCrc(189.40625);
        eph.setCrs(11.96875);
        eph.setCic(3.35276126862e-08);
        eph.setCis(-6.14672899246e-08);
        eph.setCuc(5.60656189919e-07);
        eph.setCus(1.02929770947e-05);
        eph.setSquareA(5153.65583992);
        eph.setEc(0.00892822921742);
        eph.setM0(2.82771526885);
        eph.setOmega0(3.05241315139);
        eph.setOmega(0.71538381991);
        eph.setOmegaDot(-7.64924719328e-09);
        eph.setI0(0.976218729714);
        eph.setIdot(5.63237746827e-10);
        eph.setDeltaN(3.88230457071e-09);

        SatellitePositionGNSS satellitePosition = new SatellitePositionGNSS(eph, tow, approxUserCoord, options);
        satellitePosition.computeSatellitePosition(tow);
        //System.out.println("" + (tow) + " " + satellitePosition.satCoordinates);

        satellitePosition.applyEarthRotation(0.07);

        double dist = Utils.distanceBetweenCoordinates(trueSatCoordinate, satellitePosition.getSatCoordinates());

        System.out.println("" + dist + ", " + satellitePosition.getDtSat());
    }

}

